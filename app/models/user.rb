# == Schema Information
# Schema version: 20160207204353
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  first_name             :string
#  last_name              :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  slug                   :string
#  postal_code            :string
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  has_paper_trail

  extend FriendlyId

  ZIP_POSTAL_CODE = /(^\d{5}(-\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$)/

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  friendly_id :slug_candidates, use: [:slugged, :history]

  rolify

  after_create :_set_default_role

  validates :first_name, :last_name, :email, :postal_code, presence: true
  validates :postal_code, format: { with: ZIP_POSTAL_CODE }

  # Try building a slug based on the following fields in
  # increasing order of specificity.
  def slug_candidates
    [
      :first_name,
      [:first_name, :last_name]
    ]
  end

  private

  def _set_default_role
    self.add_role :student
  end
end
