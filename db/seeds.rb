# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# User.create(email: Rails.application.secrets.foundation_email,
#             password: Rails.application.secrets.foundation_password, password_confirmation: Rails.application.secrets.foundation_password,
#             confirmed_at: Time.now)

