peeach
======

[![Codacy Badge](https://api.codacy.com/project/badge/grade/92f55ebc32e24076bfee23d05b451e3f)](https://www.codacy.com)
[![Codeship Status for peeach/peeach](https://codeship.com/projects/95f453b0-7b72-0133-cc62-56b188ef2fa7/status?branch=master)](https://codeship.com/projects/119578)

# Dev Setup

* Install Ruby 2.3.0
  `rvm install ruby-2.3.0`
* Install foreman
  `gem install foreman`
* Start server
  `foreman start`

## Mailcatcher Setup

* Install mailcatcher into dedicated gemset
  `rvm default@mailcatcher --create do gem install mailcatcher`
  `rvm wrapper default@mailcatcher --no-prefix mailcatcher catchmail`
* Start server will also start up mailcatcher
* Visit http://127.0.0.1:1080 or {machine-ip-address}:1080