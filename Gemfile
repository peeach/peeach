source 'https://rubygems.org'

ruby "2.3.0"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18.4'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use bootstrap 3
gem 'bootstrap-sass', '~> 3.3', '>= 3.3.6'
# Add browser vendor prefixes automatically
gem 'autoprefixer-rails', '~> 6.1', '>= 6.1.1'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
# Use bootstrap form
gem 'simple_form', '~> 3.2', '>= 3.2.1'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.6'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'devise', '~> 4.2'
gem 'pundit', '~> 1.1'
gem 'rolify', '~> 5.1'

# Use PaperTrail to have change tracking
gem 'paper_trail', '~> 5.2'

# Use Puma as the app server
gem 'puma', '~> 3.5', '>= 3.5.2'

# User friendly_id to generate pretty URL
gem 'friendly_id', '~> 5.1.0'

# Rails engine for static pages
gem 'high_voltage', '~> 3.0'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development do
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '~> 1.4', '>= 1.4.4'
  # Spring command for rspec
  gem 'spring-commands-rspec', '~> 1.0', '>= 1.0.4'

  # Bootstrap
  gem 'rails_layout', '~> 1.0', '>= 1.0.29'

  # Annotate table
  gem 'annotate', '~> 2.6', '>= 2.6.10'

  # Notify N+1 queries
  gem 'bullet', '~> 4.14', '>= 4.14.10'

  gem 'guard-rspec', '~> 4.6', '>= 4.6.4', require: false
  gem 'guard-annotate', '~> 2.2'

  gem 'awesome_print', '~> 1.6', '>= 1.6.1', require: 'ap'
  gem 'hirb', '~> 0.7.3'

  # Better error message
  gem 'better_errors', '~> 2.1', '>= 2.1.1'
  gem 'binding_of_caller', '~> 0.7.2'
  gem 'quiet_assets', '~> 1.1'

  # Generate ERD diagram
  gem 'rails-erd', '~> 1.4', '>= 1.4.4'
end

group :development, :test do
  gem 'rspec-rails', '~> 3.4'
  gem 'rspec-collection_matchers', '~> 1.1', '>= 1.1.2'
  gem 'rspec-activemodel-mocks', '~> 1.0', '>= 1.0.1'

  # Use factory_girl for fixture replacement
  gem 'factory_girl_rails', '~> 4.5'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', '~> 8.2', '>= 8.2.2'

  # Access an IRB console exception pages or by using <%= console %>
  gem 'web-console', '~> 2.2', '>= 2.2.1'

  # Use faker to generate fake data
  gem 'faker', '~> 1.6', '>= 1.6.1'
end

group :test do
  gem 'shoulda-matchers', '~> 3.0'
  gem 'database_cleaner', '~> 1.5', '>= 1.5.1'
  gem 'capybara', '~> 2.5'
  gem 'launchy', '~> 2.4', '>= 2.4.3'
  gem 'poltergeist', '~> 1.8', '>= 1.8.1'
  gem 'selenium-webdriver', '~> 2.48', '>= 2.48.1'

  # Code coverage analysis
  gem 'simplecov', '~> 0.10.0', require: false
  gem 'simplecov-rcov', '~> 0.2.3'
  gem 'codacy-coverage', require: false
end

group :production do
  gem 'rails_12factor'
end
