# www.rubydoc.info/gems/factory_girl/file/GETTING_STARTED.md

RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods

  config.before(:suite) do
    begin
      DatabaseCleaner.start
      # TODO Test fails for user postal code
      # FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
  end
end

